import chalk from 'chalk';
import fs from 'fs';


const fse = require('fs-extra');

async function readWriteSync(options) {
    var data = fs.readFileSync('./jobs/' + options.job_name + '/' + options.job_name + '.yml', 'utf-8'); 
    var datas = fs.readFileSync('./jobs/' + options.job_name + '/job.yml', 'utf-8'); 
    var newValue = data.replace('job_name', options.job_name); 
    var newValues = datas.replace('job_name', options.job_name); 
    fs.writeFileSync('./jobs/' + options.job_name + '/' + options.job_name + '.yml', newValue, 'utf-8'); 
    fs.writeFileSync('./jobs/' + options.job_name + '/job.yml', newValues, 'utf-8'); 
  }

export async function createJob(options) {
 options = {
      ...options,
    //  targetDirectory: options.targetDirectory || './jobs',
 };
//  const currentFileUrl = import.meta.url;
//  const templateDir = path.resolve(
//     new URL(currentFileUrl).pathname,
//     '../../job_template',
//   );
//   options.templateDirectory = templateDir;
 console.log('📋 Copying the job\'s template');
 
 fse.copySync('./tools/job_template/job_name', './jobs/' + options.job_name)
 fse.rename('./jobs/' + options.job_name + '/job_name.yml', './jobs/' + options.job_name + '/' + options.job_name + '.yml')
 await readWriteSync(options);
 console.log('%s Job ready', chalk.green.bold('DONE'));
 return true;
}