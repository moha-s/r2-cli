import arg from 'arg';
import inquirer from 'inquirer';
import { createJob } from './main';

function parseArgumentsIntoOptions(rawArgs) {
 const args = arg({});
 return {
   job_name: args._[0],
 };
}

async function promptForMissingOptions(options) {
    const questions = [];
    if (!options.job_name) {
      questions.push({
        type: 'input',
        name: 'job_name',
        message: 'Please choose which job name to use',
      });
    }
   
    const answers = await inquirer.prompt(questions);
    return {
      ...options,
      job_name: options.job_name || answers.job_name,
    };
   }
   
   export async function cli(args) {
    let options = parseArgumentsIntoOptions(args);
    options = await promptForMissingOptions(options);
    await createJob(options);
   }