# Create a job folder from R2DevOps job template

## Install

```
$ npm install create-job
```

## Usage

```s
~/hub$ create-job
```

OR  

```s
~/hub$ create-job <job_name>
```